var slideout = new Slideout({
    'panel': document.getElementById('panel'),
    'menu': document.getElementById('mobile-menu'),
    'padding': 256,
    'tolerance': 70,
	'side': 'right'
});
slideout.close();
document.querySelector('.toggle-button').addEventListener('click', function() {
	slideout.toggle();
});

function duplicate_menu_to_mobile() {
	var main_menu = $('#main-nav ul').html();
	var new_menu = `<ul class="menu vertical">${main_menu}</ul>`;
	$('#mobile-menu > .menu-section').html(new_menu);
	$('#mobile-menu .mobile-trigger').remove();
	$('#mobile-menu .ghost').removeClass('ghost');
}
$(document).ready(function() {
	console.log('so ready');
	duplicate_menu_to_mobile();
});
